#!/usr/bin/env bash
#!/usr/bin/env bash

##########################################################################
# AUTHOR : Mika                                                          #
# Description : Ce script permets de d'activer ou désactiver le démarrage#  
# automatique d'un service en utilisant SYSVINIT                         #
# Version : 0.0.1                                                        #
##########################################################################

#USAGE : Simple utils 


function delimiter(){
	
	echo '+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
}

function banner() {
	delimiter
	figlet -f small rcconfmanager 
	delimiter
}

function printOK() {
		messages=$2
		
		if [ "$1"=="0" ] 
		then
			delimiter
			echo -e "\n"
			echo "$messages:[OK]"
			echo -e "\n"
			
			logger "$messages:[OK]"
	
		else
			delimiter
			echo -e "\n"
			echo "$messages:[NOK]"
			echo -e "\n"
			
			logger "$messages:[NOK]"
		fi

}

function pause(){
	echo "Appuyer sur Entree pour continuer..."
	read
}

function debug {
		#arg0 true or false
		
		if [ "$1" ="0" ] 
		then 
			set -x 
			
		else
			set +x
		fi
}
