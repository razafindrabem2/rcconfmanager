#!/usr/bin/env bash

##########################################################################
# AUTHOR : Mika                                                          #
# Description : Ce script permets de d'activer ou désactiver le démarrage#  
# automatique d'un service en utilisant SYSVINIT                         #
# Version : 0.0.1                                                        #
##########################################################################

# Menu Principal

ici=$(dirname $0)

. $ici/utils.sh



clear

banner

debug "0"1

while (true)
do
	clear
	banner
	echo ''
	echo 'MENU: '
	echo -e "\t[1]: Pour démarrer un service automatiquement "
	echo -e "\t[2]: Pour désactiver le démarrage automatique d'un service  "
	echo -e "\t[q]: Pour quitter "
	
	read -p 'Choix:' reponse
	
	case $reponse in 
	'1')
		. $ici/manage.sh "OFF"
	;;
	'2')
		. $ici/manage.sh "ON"
	;;

	'q'|'Q'|'exit'|'quit')
		exit 1
	;;
	*)
		echo 'Fonctionnalités pas encore dévelopé'
	;;
	esac
	pause
done
