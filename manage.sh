#!/usr/bin/env bash

##########################################################################
# AUTHOR : Mika                                                          #
# Description : Ce script permets de d'activer ou désactiver le démarrage#  
# automatique d'un service en utilisant SYSVINIT                         #
# Version : 0.0.1                                                        #
##########################################################################

# 0 : ACTIVATE ALL DISABLE SCRIPTS
# 1 : DISABLE AT STARTUP


ici=$(dirname $0)
. $ici/dialogs_utils.sh

tempfile=`(tempfile) 2>/dev/null` || tempfile="/tmp/rcmanager$$"
trap "rm -f $tempfile" 0 $SIG_NONE $SIG_HUP $SIG_INT $SIG_QUIT $SIG_TERM

OPTIONS=()

function checkservice {
	name=$1
	is_exist="ON"
	
	tmp="/tmp/exist"
	
	((ls /etc/rc{0..6}.d ; ls /etc/rcS.d 1>/dev/null 2>&1)  | grep "$name" | grep "^S" | wc -l )  1>$tmp 2>&1
	count=$(cat $tmp)
	
	rm $tmp 
	
	if [[ "$count" == "0" ]]
	then
			is_exist="OFF"
			
	fi
	
}

function manageservice {
	c=$1
	name=$2
		
			
	if [ "$c" == "OFF" ] 
	then 
		 sudo update-rc.d  $name enable 1>/dev/null 2>&1
		 printOK $? "Ajout du service $name au démarrage ... "
	else
		if [ "$c" == "ON" ]
		then 
			sudo update-rc.d $name disable 1>/dev/null 2>&1
			printOK $? "Désactivation du service $name au démarrage ..."
		fi
	fi
}

function doTask {

	choix=$1
	let i=1
	
	for list in $(ls /etc/init.d)
	do 
		checkservice $list
		if [ "$is_exist" == "$choix" ]
		then
			OPTIONS+=($list $i "OFF")
			let i+=1 
		fi
	done


	#echo "${OPTIONS[@]}" > "/tmp/a.txt"
	

	[ "$choix" == "OFF" ] \
		&& description="Selectionner le service que vous voulez démarrer automatiquement" \
		|| description="Selectionner le service que vous voulez désactiver le démarrage automatique"

	$DIALOG --backtitle "Service Startup Manager : Sysvinit" \
		--clear \
		--title "LISTES DES SERVICES"  \
			--checklist "$description" 20 71 20 \
			"${OPTIONS[@]}" 2> $tempfile
	
	retval=$?

	case $retval in
	  $DIALOG_OK)
		clear
		
		for name in $(cat $tempfile)
		do
			manageservice $choix $name
		done
		;;
		
	  $DIALOG_CANCEL)
		clear;;
	  $DIALOG_HELP)
		echo "Help pressed: `cat $tempfile`";;
	  $DIALOG_EXTRA)
		echo "Extra button pressed.";;
	  $DIALOG_ITEM_HELP)
		echo "Item-help button pressed: `cat $tempfile`";;
	  $DIALOG_ESC)
		if test -s $tempfile ; then
		  clear
		  cat $tempfile
		  exit 1
		else
		  clear
		  echo "ESC pressed."
		fi
		;;
	esac

}

clear 
 
doTask $1

